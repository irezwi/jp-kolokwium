import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class AppWindow extends JFrame {

    public static final long serialVersionUID = 1L;
    public JPanel contentPane;


    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                final AppWindow frame = new AppWindow();
                frame.setVisible(true);
                frame.setBackground(Color.WHITE);
            } catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    public AppWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int windowWidth = 450;
        int windowHeight = 300;
        setBounds((screen.width-windowWidth)/2, (screen.height-windowHeight)/2, windowWidth, windowHeight);
        contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(null);

        AnimatorPanel animPanel = new AnimatorPanel();
        animPanel.setBounds(10, 11, 415, 219);
        animPanel.setBackground(Color.BLACK);
        contentPane.add(animPanel);
        SwingUtilities.invokeLater(animPanel::initialize);

        JButton btnAdd = new JButton("Add");
        btnAdd.addActionListener(e -> animPanel.addFig());
        btnAdd.addActionListener(e -> animPanel.animate());
        btnAdd.setBounds(10, 239, 80, 23);
        contentPane.add(btnAdd);

    }
}
