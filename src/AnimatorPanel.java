import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnimatorPanel extends JPanel implements ActionListener {


    Image image;
    Graphics2D device;
    Graphics2D buffer;
    private int delay = 70;
    public static Timer timer;

    public AnimatorPanel() {
        super();
        setBackground(Color.WHITE);
        timer = new Timer(delay, this);
        addKeyListener(new KAdapter());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        device.drawImage(image, 0, 0, null);
        buffer.clearRect(0, 0, getWidth(), getHeight());
    }

    public void addFig() {
        MyShape fig = new Circle(buffer, delay, getWidth(), getHeight());
        timer.addActionListener(fig);
        new Thread(fig);
    }

    public void initialize() {
        int width = getWidth();
        int height = getHeight();

        image = createImage(width, height);
        buffer = (Graphics2D) image.getGraphics();
        buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        buffer.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        device = (Graphics2D) getGraphics();
        device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public void animate() {
        if (timer.isRunning()) {
            timer.stop();
        } else {
            timer.start();
        }
    }
}
