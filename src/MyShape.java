import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Area;
import java.util.Random;

public abstract class MyShape implements Runnable, ActionListener {
    protected Graphics2D buffer;
    protected Area area;
    protected Shape shape;

    private int dx, dy;
    private int delay;
    private int width;
    private int height;
    private Color color;
    protected static final Random rand = new Random();


    public MyShape(Graphics2D buffer, int delay, int width, int height) {
        this.delay = delay;
        this.buffer = buffer;
        this.width = width;
        this.height = height;
        color = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(155) + 90);
    }

    public void run() {
        shape = area;

        while (true) {
            if (AnimatorPanel.timer.isRunning()) {
                shape = nextFrame();
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void move() {

    }

    private Shape nextFrame() {
        area = new Area(area);
        Rectangle bounds = area.getBounds();

        return area;
    }

}
